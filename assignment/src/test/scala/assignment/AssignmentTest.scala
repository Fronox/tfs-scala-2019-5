package assignment

import bcrypt.AsyncBcryptImpl
import com.typesafe.config.ConfigFactory
import org.scalatest.Matchers._
import org.scalatest._

import scala.concurrent.ExecutionContext

class AssignmentTest extends AsyncFlatSpec {

  val config = ConfigFactory.load()
  val credentialStore = new ConfigCredentialStore(config)
  val reliableBcrypt = new AsyncBcryptImpl
  val assignment = new Assignment(reliableBcrypt, credentialStore)(ExecutionContext.global)

  import assignment._

  behavior of "verifyCredentials"

  it should "return true for valid user-password pair" in {
    verifyCredentials("winnie", "pooh").map { result =>
      result shouldBe true
    }
  }

  it should "return false if user does not exist in store" in ???
  it should "return false for invalid password" in ???

  behavior of "withCredentials"

  it should "execute code block if credentials are valid" in ???
  it should "not execute code block if credentials are not valid" in ???

  behavior of "hashPasswordList"

  it should "return matching password-hash pairs" in ???

  behavior of "findMatchingPassword"

  it should "return matching password from the list" in ???
  it should "return None if no matching password is found" in ???

  behavior of "withRetry"

  it should "return result on passed future's success" in ???
  it should "not execute more than specified number of retries" in ???
  it should "not execute unnecessary retries" in ???
  it should "return the first error, if all attempts fail" in ???

  behavior of "withTimeout"

  it should "return result on passed future success" in ???
  it should "return result on passed future failure" in ???
  it should "complete on never-completing future" in ???

  behavior of "hashPasswordListReliably"
  val assignmentFlaky = new Assignment(new FlakyBcryptWrapper(reliableBcrypt), credentialStore)(ExecutionContext.global)

  it should "return password-hash pairs for successful hashing operations" in ???
}
